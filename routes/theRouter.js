const express = require("express");
const Employees = require('../models/employees');
const bodyParser = require('body-parser');

const theRouter = express.Router();

theRouter.use(bodyParser.json());


theRouter.route("/")
.get((req, res) => {
  Employees.find({})
  .then(
    (employees) => {
      res.setHeader("Content-Type", "application/json");
      res.json(employees);
    },
    (err) => next(err)
  ).catch((err) => console.log(err));
})
.post((req, res) => {
  Employees.create(req.body)
    .then((employee) => {
        console.log('Employee Added ', employee);
        res.setHeader('Content-Type', 'application/json');
        res.json(employee);
    }, (err) => console.log(err))
})
.put((req, res, next) => {
  console.log(req.body)
  Employees.findByIdAndUpdate( req.body._id, {
    firstName:req.body.firstName,
    lastName:req.body.lastName,
    age: req.body.age,
  }, { new: true }) 
  .then((employee) => {
      res.setHeader('Content-Type', 'application/json');
      console.log(employee)
  }, (err) => console.log(err))
})
.delete((req, res, next) => {
  Employees.findOneAndDelete({firstName: req.body.firstName})
    .then((resp) => {
      res.setHeader('Content-Type', 'application/json');
      res.json('deleted');
    }, (err) => console.log(err))
}); 

module.exports = theRouter;
