const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const employeeSchema = new Schema ({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true
    },
    age: {
        type: String,
        required: true
    }
},{
    timestamps: true //createdAt, updatedAt 
})

var Employees = mongoose.model('employee', employeeSchema)

module.exports = Employees