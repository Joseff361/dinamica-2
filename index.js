const express = require("express");
const http = require("http");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const theRouter = require("./routes/theRouter");
const mongoose = require("mongoose");

const hostname = "localhost";
const port = 3000;


// mongoose
const url = "mongodb+srv://jobby:jobby@cluster0.ylblz.mongodb.net/DAD?retryWrites=true&w=majority";
const connect = mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true  });

connect
.then((cb) => {
  console.log("Connected correctly to server");
})
.catch((err) => console.log(err));

const app = express();

app.use(morgan("dev"));
app.use(express.static(__dirname + "/public"));
app.use("/employee", theRouter);

const server = http.createServer(app);
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}`);
});
